// Load env values
const prefix = process.env.PREFIX;
const guildId = process.env.GUILD_ID;

// at the top of your file
const Discord = require('discord.js');

module.exports = {
  name: 'welcome', // command name
  sendTo(user, guild) {
    // Lookup guild by .env GUILD_ID
    // const guild = message.client.guilds.cache.get(guildId);

    // Find correct role in guild
    const guildRole = guild.roles.cache.find(
      (role) => role.name === 'probation'
    );

    const authorizedRole = guild.roles.cache.find(
      (role) => role.name === 'authorized'
    );

    // get the userID of the user
    const memberId = user.id;

    // look up record of guild member by userID.
    guild.members.fetch(memberId).then((member) => {
      //add guildRole to the guild member
      member.roles.add(guildRole);
      member.roles.remove(authorizedRole);
    });
  },
  execute(message) {
    const data = [];

    return;
  },
};
