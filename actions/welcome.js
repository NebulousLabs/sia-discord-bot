// This file is responsible for creating and writing the welcome message
// in the channel where `w` command is called.

// This file loads content from 'pollOptions.json' and 'welcomeMessage.js' and also relies on
// googleCreds.json and some environmental variables, listed just below.

// Load env values
const prefix = process.env.PREFIX;
const googleSpreadsheetId = process.env.GOOGLE_SHEET_ID;
const guildId = process.env.GUILD_ID;

//Load polling options
const {
  welcomeTitle,
  welcomeDescription,
  ruleList,
  channelsMessage,
} = require('../customization/welcomeMessage.json');
//Load polling options
const { pollOptionsList } = require('../customization/pollOptions.json');

// load Discordjs code
const Discord = require('discord.js');

const { GoogleSpreadsheet } = require('google-spreadsheet');

// build list of emojis from poll options
const reactions = pollOptionsList.map((role) => role.emoji);

// filter reactions to only those of the user
// Discord
const reactionFilter = (reaction) => {
  let validEmoji = null;

  if (reaction._emoji.id) {
    validEmoji = reactions.includes(
      `<:${reaction._emoji.name}:${reaction._emoji.id}>`
    )
      ? `<:${reaction._emoji.name}:${reaction._emoji.id}>`
      : validEmoji;
  } else {
    validEmoji = reactions.includes(`${reaction._emoji.name}`)
      ? `${reaction._emoji.name}`
      : validEmoji;
  }

  let value = null;

  if (validEmoji) {
    const op = pollOptionsList.filter((choice) =>
      choice.emoji.includes(validEmoji)
    );

    value = op[0].value;
  }

  return value;
};

// For a specific user in guild, adds role "Unverified"

const addUserUnverified = (user, guild) => {
  // Find correct role in guild
  const guildRole = guild.roles.cache.find(
    (role) => role.name === 'Unverified'
  );

  // get the userID of the user
  const memberId = user.id;

  // look up record of guild member by userID.
  guild.members.fetch(memberId).then((member) => {
    //add guildRole to the guild member
    member.roles.add(guildRole);
  });
};

// For a specific user in guild, removes role "Unverified"
// If role not present, do nothing.

const removeUserUnverified = (user, guild) => {
  // Find correct role in guild
  const guildRole = guild.roles.cache.find(
    (role) => role.name === 'Unverified'
  );

  // get the userID of the user
  const memberId = user.id;

  // look up record of guild member by userID.
  guild.members
    .fetch(memberId)
    .then((member) => {
      //add guildRole to the guild member
      member.roles.remove(guildRole).catch(() => {});
    })
    .catch(() => {});
};

// Appends new row for user response on Google Sheets
const appendReaction = async (reaction, user) => {
  // console.log(user);
  // console.log(reaction);
  let responseItem = { value: 'test' };
  let reactionEmoji = reactionFilter(reaction);

  if (reactionEmoji) {
    responseItem.value = reactionEmoji;
  } else {
    // delete reaction!
    reaction
      .remove()
      .catch((error) => console.error('Failed to remove reactions: ', error));
    return;
  }

  // spreadsheet key is the long id in the sheets URL
  const doc = new GoogleSpreadsheet(googleSpreadsheetId);

  //Log response to console for pm2 and debug.
  console.log(
    `${user.username}#${user.discriminator} responded: ${responseItem.value}`
  );

  const guild = reaction.message.client.guilds.cache.get(guildId);

  removeUserUnverified(user, guild);

  //Add to spreadsheet

  await doc.useServiceAccountAuth(require('../googleCreds.json'));
  await doc.loadInfo(); // loads document properties and worksheets

  //use first sheet
  const sheet = doc.sheetsByIndex[0];

  let date = new Date();

  // write to Google Sheets
  await sheet.addRow([
    user.username,
    user.username + '#' + user.discriminator,
    user.id,
    responseItem.value,
    date.toLocaleString('en-US', {
      timeZone: 'America/New_York',
      timeZoneName: 'short',
    }),
  ]);

  return;
};

const rules = ['1.'];

module.exports = {
  name: 'welcome', // command name
  appendReaction,
  addUserUnverified,
  execute(message, showPoll) {
    const data = [];

    const channel = message.channel;

    // Format lines for the embed by iterating through pollOptionsList
    pollLines = pollOptionsList.map((option) => {
      return `${option.emoji}  ${option.label}\n\n`;
    });

    // Format lines for the rules by iterating through ruleList
    ruleLines = ruleList.map((option) => {
      return `${option}\n\n`;
    });

    // Build embed to send to user.
    const embed = new Discord.MessageEmbed()
      .setColor('#58b560')
      .setTitle(welcomeTitle)
      .setDescription(welcomeDescription)
      .setThumbnail(
        'https://siasky.net/EADRX8pgGnVWLKl-AyB80pBhcQSZUVycO6g-ySuIXv-MLw'
      )
      .addField('Rules', ruleLines.join(''))
      .addField('Channels', `${channelsMessage}`);

    // if we're not showing the poll, we want resources channel and return early
    if (!showPoll) {
      embed.addField(
        'Resources',
        `See the #resources channel for additional links and information.`
      );
      return channel.send(embed);
    }

    // if we do need to show the poll show quick-links channel then the poll.
    embed
      .addField(
        'Resources',
        `See the #quick-links channel for additional links and information.\n\n\u200b`
      )
      .addField(
        'Where did you find out about Sia/Skynet?',
        `${pollLines.join('')}`
      );

    channel.send(embed);

    data.push(
      'Please select a reaction below to let us know and access the server!'
    );

    // Send message, reactions and then ID of message to set in .env.production.local and manually deleted
    channel.send(data).then(async (m) => {
      // create a list of functions, each returns promise and is an emoji reaction
      const reactionActions = reactions.map((reaction) => m.react(reaction));

      // Different method than in chan.js, here all reactions in list above, when resolved
      // will get wrapped in promise and succeed or fail.
      // Also, ordering is not guaraneed since they're done async.

      Promise.all(reactionActions).catch(() =>
        console.error('One of the emojis failed to react.')
      );

      channel.send(m.id);
    });

    return;
  },
};
