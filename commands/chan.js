// const { prefix, guildId } = require('../config.json');
const prefix = process.env.PREFIX;
const guildId = process.env.GUILD_ID;

const { roleOptionsList } = require('../customization/roleOptions.json');

const rolesList = roleOptionsList;

module.exports = {
  name: 'chan', // command name
  aliases: ['c'], // aliases for command name
  cooldown: 5, // amount of time in seconds user will have to wait before running again.
  args: false, // return usage info if no args given
  guildOnly: false, // can command be used in DMs?
  usage: '', //text helping provide usage info
  description: 'Sends user to DM to add to channel.', // Help description
  execute(message, args) {
    const data = [];

    // if no args
    if (!args.length) {
      //Build message to send to user

      data.push('I see you want to be added to a channel!');
      data.push('');

      // iterate through roles list, line for each
      rolesList.map((role) => {
        data.push(` - ${role.emoji} **${role.channelString}**`);
      });

      data.push('');
      data.push('Please select a reaction below to be added.');

      // array of just the emoji reactions
      const reactions = rolesList.map((role) => role.emoji);

      // filter to confirm that reaction received is in our list and done by user
      const filter = (reaction, user) => {
        return (
          reactions.includes(reaction.emoji.name) &&
          user.id === message.author.id
        );
      };

      // Send DM asking for channel to be added to
      message.author.send(data).then(async (m) => {
        // React to message with each emoji in list
        await reactions.reduce(async (_, emoji) => {
          await m.react(emoji);
        }, undefined);

        // Setup reaction collector: timesout at 30 seconds, max 1 response
        m.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] })
          .then((collected) => {
            // since only 1 response allowed, next() gets first and only item in collection
            const roleSelected = collected.keys().next().value;

            // if roleSelected is truthy
            if (!!roleSelected) {
              //look up roleName from our list using emoji
              // (for discord emojis, code from poll.js might work better)
              const roleName = rolesList.filter(
                (r) => r.emoji === roleSelected
              )[0];

              // Lookup guild by .env GUILD_ID
              const guild = message.client.guilds.cache.get(guildId);

              // Find correct role in guild
              const guildRole = guild.roles.cache.find(
                (role) => role.name === roleName.role
              );

              // get the userID of the user
              const memberId = message.author.id;

              // look up record of guild member by userID
              guild.members.fetch(memberId).then((member) => {
                //add guildRole to the guild member
                member.roles.add(guildRole);
              });

              // write request to console for pm2 and debugging
              console.log(
                `${message.author.username}#${message.author.discriminator} requests role ${roleName.channelString}`
              );

              // Respond with message to the user if successful.
              message.author.send(
                `Great! You've been added to the **${roleName.emoji} ${roleName.channelString}** channel. Type \`${prefix}${this.name}\` to join another channel.`
              );
            } else {
              message.author.send(`Something went wrong.`);
            }
          })
          .catch((err) => {
            message.author.send(
              `I didn't get a response or something went wrong. Type \`${prefix}${this.name}\` to try again.`
            );
          });
      });

      return;
    }
  },
};
