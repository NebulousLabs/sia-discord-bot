const skynet = require('skynet-js');

const skynetAction = async (message, client, args) => {
  try {
    const data = await client.resolveHns(args[0]);
    console.log(typeof data);
    console.log(data);

    // TODO: Fully resolve registry entries.
    if ('registry' in data) {
      // return message.channel.send(
      //   'Registry HNS entries ("skyns:") not currently supported.'
      // );
      // const { publickey, datakey } = data['registry'];
      const publickey =
        'ed25519:f0ce2a794d1dd38227fe2d53715e1170a47d0b76881cd3d702b1a4300c484aa5';
      const datakey =
        'f829b781bfd5507ae548ef5230af9c64e76d164385c38b1d15d6f96595fc7c3d';

      console.log('reg data');
      console.log(publickey);
      console.log(datakey);

      const entry = await client.registry.getEntry(publickey, datakey);

      console.log('entry!');
      console.log(entry);

      if (entry) {
        return message.channel.send(entry);
      }
    } else if ('skylink' in data) {
      console.log('skylink');
      const skylink = data['skylink'];
      const url = await client.getSkylinkUrl(skylink);
      return message.channel.send(`${url}`);
    } else {
      return message.channel.send('No HNS record found.');
    }
  } catch (err) {
    console.log(err.message);
    return message.channel.send('something messed up.');
  }
};

module.exports = {
  name: 'hnsres',
  description: 'Resolve an HNS TLD to a skylink.',
  devOnly: true, // only bot with 'dev' value for BOT env variable will run
  execute(message, args) {
    const client = new skynet.SkynetClient('https://siasky.net');

    skynetAction(message, client, args);
  },
};
