// ## This is an early version of a poll, which has been replaced by the welcome message. If we need to revert to DM message, this is a good starting place.

const prefix = process.env.PREFIX;

const Poll = require('../actions/poll');

module.exports = {
  name: 'poll', // command name
  aliases: ['p'], // aliases for command name
  cooldown: 5, // amount of time in seconds user will have to wait before running again.
  args: false, // return usage info if no args given
  devOnly: true, // if true only bot with 'dev' value for BOT env variable will run
  guildOnly: false, // can command be used in DMs?
  usage: '', //text helping provide usage info
  description: 'Asks new user poll. Command is for testing purposes.', // Help description
  execute(message, args) {
    return Poll.execute(message.author);
  },
};
