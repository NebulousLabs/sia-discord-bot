// This servers as the template for other commands.

module.exports = {
  name: 'delete', // command name
  aliases: ['del', 'd'], // aliases for command name
  cooldown: 5, // amount of time in seconds user will have to wait before running again.
  args: true, // return usage info if no args given
  guildOnly: false, // can command be used in DMs?
  devOnly: true, // only bot with 'dev' value for BOT env variable will run
  usage: '<n>', //text helping provide usage info
  description: 'Bulk Delete Messages.', // Help description
  execute(message, args) {
    const amount = parseInt(args[0]);

    if (isNaN(amount)) {
      return message.reply("that doesn't seem to be a valid number.");
    } else if (amount < 2 || amount > 100) {
      return message.reply('you need to input a number between 2 and 100.');
    }

    message.channel.bulkDelete(amount, true);
  },
};
