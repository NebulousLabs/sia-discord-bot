module.exports = {
  name: 'ping',
  description: 'Ping!',
  devOnly: true, // if true only bot with 'dev' value for BOT env variable will run
  execute(message, args) {
    message.channel.send('Pong.');
  },
};
